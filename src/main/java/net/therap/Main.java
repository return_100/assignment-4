package net.therap;

import net.therap.controller.AdminController;
import net.therap.controller.HomeController;
import net.therap.controller.TraineeController;
import net.therap.util.EntityManagerUtil;
import net.therap.view.MessageView;

import static net.therap.view.Messages.*;

/**
 * @author al.imran
 * @since 13/04/2021
 */
public class Main {

    private static final String ADMIN = "Admin";
    private static final String TRAINEE = "Trainee";

    public static void main(String[] args) {

        while (true) {
            String role = new HomeController().getRole();
            EntityManagerUtil.init();

            if (ADMIN.equals(role)) {
                new AdminController().getService();
            } else if (TRAINEE.equals(role)) {
                new TraineeController().getService();
            } else {
                MessageView.show(EXIT);
                break;
            }

            EntityManagerUtil.closeEntityManager();
        }
    }
}
