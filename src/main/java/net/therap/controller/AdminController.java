package net.therap.controller;

import net.therap.model.Course;
import net.therap.service.AdminService;
import net.therap.view.MessageView;

import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import static net.therap.view.Messages.*;

/**
 * @author al.imran
 * @since 15/04/2021
 */
public class AdminController {

    private static final int MIN = 1;
    private static final int MAX = 5;

    private static final int LOGIN = 1;
    private static final int SHOW_ALL_COURSE = 2;
    private static final int ADD_COURSE = 3;
    private static final int DELETE_COURSE = 4;
    private static final int BACK = 5;

    public void getService() {
        boolean isLoggedIn = false;
        AdminService adminService = new AdminService();

        while (true) {

            MessageView.show(ADMIN_OPTIONS);
            Scanner scn = new Scanner(System.in);
            int option = scn.nextInt();

            if (option >= MIN && option <= MAX) {

                if (option == LOGIN) {
                    MessageView.show(ENTER_EMAIL);
                    String email = scn.next();

                    MessageView.show(ENTER_PASSWORD);
                    String password = scn.next();

                    if (Objects.nonNull(adminService.login(email, password))) {
                        isLoggedIn = true;
                        MessageView.show(SUCCESSFUL_LOGIN);
                    } else {
                        isLoggedIn = false;
                        MessageView.show(UNSUCCESSFUL_LOGIN);
                    }
                }

                if (option == SHOW_ALL_COURSE && isLoggedIn) {
                    MessageView.show(COURSE_LIST);
                    List<Course> courses = adminService.getAllCourse();

                    for (Course course : courses) {
                        MessageView.show(course.toString());
                    }
                }

                if (option == ADD_COURSE && isLoggedIn) {
                    MessageView.show(ENTER_COURSE_NAME);
                    String coursename = scn.next();

                    adminService.addCourse(coursename);
                }

                if (option == DELETE_COURSE && isLoggedIn) {
                    MessageView.show(ENTER_COURSE_ID);
                    int courseId = scn.nextInt();

                    MessageView.show(ENTER_COURSE_NAME);
                    String coursename = scn.next();

                    adminService.deleteCourse(courseId, coursename);
                }

                if (option == BACK) {
                    break;
                }
            }
        }
    }
}
