package net.therap.controller;

import net.therap.view.MessageView;
import net.therap.view.Messages;

import java.util.Scanner;

/**
 * @author al.imran
 * @since 15/04/2021
 */
public class HomeController {

    private static final int MIN = 1;
    private static final int MAX = 3;
    private static final int ADMIN_ROLE = 1;
    private static final int TRAINEE_ROLE = 2;

    public String getRole() {
        while (true) {
            MessageView.show(Messages.HOME_OPTIONS);
            Scanner scn = new Scanner(System.in);
            int role = scn.nextInt();

            if (role >= MIN && role <= MAX) {
                if (role == ADMIN_ROLE) {
                    return "Admin";
                } else if (role == TRAINEE_ROLE) {
                    return "Trainee";
                } else {
                    return "Exit";
                }
            }
        }
    }
}
