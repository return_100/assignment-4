package net.therap.controller;

import net.therap.model.Course;
import net.therap.service.TraineeService;
import net.therap.view.MessageView;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

import static net.therap.view.Messages.*;

/**
 * @author al.imran
 * @since 15/04/2021
 */
public class TraineeController {

    private static final int MIN = 1;
    private static final int MAX = 7;

    private static final int REGISTER = 1;
    private static final int LOGIN = 2;
    private static final int SHOW_ALL_COURSE = 3;
    private static final int SHOW_ENROLLED_COURSE = 4;
    private static final int ENROLLED_IN_COURSE = 5;
    private static final int UNENROLLED_FROM_COURSE = 6;
    private static final int BACK = 7;

    public void getService() {
        TraineeService traineeService = new TraineeService();
        boolean isLoggedIn = false;

        while (true) {

            MessageView.show(TRAINEE_OPTIONS);
            Scanner scn = new Scanner(System.in);
            int option = scn.nextInt();

            if (option >= MIN && option <= MAX) {

                if (option == REGISTER) {
                    MessageView.show(ENTER_NAME);
                    String name = scn.next();

                    MessageView.show(ENTER_EMAIL);
                    String email = scn.next();

                    MessageView.show(ENTER_PASSWORD);
                    String password = scn.next();

                    if (traineeService.register(name, email, password)) {
                        MessageView.show(SUCCESSFUL_REGISTRATION);
                    } else {
                        MessageView.show(UNSUCCESSFUL_REGISTRATION);
                    }

                    isLoggedIn = false;
                }

                if (option == LOGIN) {
                    MessageView.show(ENTER_EMAIL);
                    String email = scn.next();

                    MessageView.show(ENTER_PASSWORD);
                    String password = scn.next();

                    if (traineeService.login(email, password)) {
                        isLoggedIn = true;
                        MessageView.show(SUCCESSFUL_LOGIN);
                    } else {
                        isLoggedIn = false;
                        MessageView.show(UNSUCCESSFUL_LOGIN);
                    }
                }

                if (option == SHOW_ALL_COURSE && isLoggedIn) {
                    MessageView.show(COURSE_LIST);
                    List<Course> courses = traineeService.getAllCourses();

                    for (Course course : courses) {
                        MessageView.show(course.toString());
                    }
                }

                if (option == SHOW_ENROLLED_COURSE && isLoggedIn) {
                    MessageView.show(ENROLLED_COURSE_LIST);
                    Set<Course> enrolledCourses = traineeService.getEnrolledCourses();

                    for (Course course : enrolledCourses) {
                        MessageView.show(course.toString());
                    }
                }

                if (option == ENROLLED_IN_COURSE && isLoggedIn) {
                    MessageView.show(ENTER_COURSE_ID);
                    int courseId = scn.nextInt();

                    MessageView.show(ENTER_COURSE_NAME);
                    String courseName = scn.next();

                    traineeService.enrolledInACourse(courseId, courseName);
                }

                if (option == UNENROLLED_FROM_COURSE && isLoggedIn) {
                    MessageView.show(ENTER_COURSE_ID);
                    int courseId = scn.nextInt();

                    MessageView.show(ENTER_COURSE_NAME);
                    String courseName = scn.next();

                    traineeService.unenrolledFromACourse(courseId, courseName);
                }

                if (option == BACK) {
                    break;
                }
            }
        }
    }
}
