package net.therap.dao;

import net.therap.model.Course;
import net.therap.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al.imran
 * @since 14/04/2021
 */
public class CourseDao implements Dao<Course> {

    private static final String JPQL_FIND_ALL = "From Course";

    private static final String JPQL_DELETE = "DELETE FROM Course WHERE id = :id";

    @Override
    public List<Course> findAll() {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            return entityManager.createQuery(JPQL_FIND_ALL, Course.class).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    @Override
    public boolean delete(Course course) {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            entityManager.getTransaction().begin();
            entityManager.createQuery(JPQL_DELETE)
                    .setParameter("id", course.getId())
                    .executeUpdate();
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }

        return false;
    }
}
