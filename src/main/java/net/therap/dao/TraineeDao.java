package net.therap.dao;

import net.therap.model.Trainee;
import net.therap.util.EntityManagerUtil;

import javax.persistence.EntityManager;

/**
 * @author al.imran
 * @since 14/04/2021
 */
public class TraineeDao implements Dao<Trainee> {

    private static final String JPQL_FIND = "FROM Trainee " +
            "WHERE email = :email and password = :password";

    @Override
    public Trainee find(Trainee trainee) {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            return entityManager.createQuery(JPQL_FIND, Trainee.class)
                    .setParameter("email", trainee.getEmail())
                    .setParameter("password", trainee.getPassword())
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
