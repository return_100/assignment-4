package net.therap.service;

import net.therap.dao.AdminDao;
import net.therap.dao.CourseDao;
import net.therap.dao.Dao;
import net.therap.model.Admin;
import net.therap.model.Course;

import java.util.List;

/**
 * @author al.imran
 * @since 15/04/2021
 */
public class AdminService {

    private final Dao<Admin> adminDao;
    private final Dao<Course> courseDao;

    public AdminService() {
        this.adminDao = new AdminDao();
        this.courseDao = new CourseDao();
    }

    public Admin login(String email, String password) {
        Admin admin = new Admin();
        admin.setEmail(email);
        admin.setPassword(password);
        return adminDao.find(admin);
    }

    public List<Course> getAllCourse() {
        return courseDao.findAll();
    }

    public void addCourse(String coursename) {
        Course course = new Course();
        course.setName(coursename);
        courseDao.save(course);
    }

    public void deleteCourse(int id, String coursename) {
        Course course = new Course();
        course.setId(id);
        course.setName(coursename);
        courseDao.delete(course);
    }
}
