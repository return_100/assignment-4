package net.therap.service;

import net.therap.dao.CourseDao;
import net.therap.dao.Dao;
import net.therap.dao.TraineeDao;
import net.therap.model.Course;
import net.therap.model.Trainee;

import java.util.*;
import java.util.regex.Pattern;

/**
 * @author al.imran
 * @since 15/04/2021
 */
public class TraineeService {

    private static final String EMAIL_REGEX = "[_a-zA-Z1-9]+(\\.[A-Za-z0-9]*)*@[A-Za-z0-9]+" +
            "\\.[A-Za-z0-9]+(\\.[A-Za-z0-9]*)*";

    private Trainee trainee;
    private final Dao<Trainee> traineeDao;
    private final Dao<Course> courseDao;

    public TraineeService() {
        this.traineeDao = new TraineeDao();
        this.courseDao = new CourseDao();
    }

    public boolean register(String name, String email, String password) {
        Trainee trainee = new Trainee();
        trainee.setEmail(email);
        trainee.setName(name);
        trainee.setPassword(password);

        if (Pattern.matches(EMAIL_REGEX, email)) {
            return traineeDao.save(trainee);
        }

        return false;
    }

    public boolean login(String email, String password) {
        Trainee trainee = new Trainee();
        trainee.setEmail(email);
        trainee.setPassword(password);
        this.trainee =  traineeDao.find(trainee);
        return Objects.nonNull(this.trainee);
    }

    public List<Course> getAllCourses() {
        return courseDao.findAll();
    }

    public Set<Course> getEnrolledCourses() {
        return trainee.getEnrolledCourses();
    }

    public void enrolledInACourse(int courseId, String coursename) {
        Course course = new Course();
        course.setId(courseId);
        course.setName(coursename);
        trainee.getEnrolledCourses().add(course);
        course.getTraineeSet().add(trainee);
        traineeDao.update(trainee);
    }

    public void unenrolledFromACourse(int courseId, String coursename) {
        Course course = new Course();
        course.setId(courseId);
        course.setName(coursename);
        trainee.getEnrolledCourses().remove(course);
        course.getTraineeSet().remove(trainee);
        traineeDao.update(trainee);
    }
}
