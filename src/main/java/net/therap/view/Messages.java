package net.therap.view;

/**
 * @author al.imran
 * @since 15/04/2021
 */
public interface Messages {

    String ENTER_NAME = "Enter Your Name:";

    String ENTER_EMAIL = "Enter Your Email Address:";

    String ENTER_PASSWORD = "Enter Your Password:";

    String SUCCESSFUL_LOGIN = "Successful Login";

    String UNSUCCESSFUL_LOGIN = "Unsuccessful Login";

    String SUCCESSFUL_REGISTRATION = "Successful Registration";

    String UNSUCCESSFUL_REGISTRATION = "Duplicate or invalid credentials";

    String COURSE_LIST = "Course List:";

    String ENROLLED_COURSE_LIST = "Enrolled Course List:";

    String ENTER_COURSE_ID = "Enter Course Id:";

    String ENTER_COURSE_NAME = "Enter Course Name:";

    String EXIT = "EXIT ... !!!";

    String HOME_OPTIONS = "Enter your role (Type a number from 1 to 3):\n" +

            "1. Admin\n" +

            "2. Trainee\n" +

            "3. Exit";


    String ADMIN_OPTIONS = "Choose one of the following options (Type a number from 1 to 5):\n" +

            "1. Login\n" +

            "2. Show all courses\n" +

            "3. Add a course\n" +

            "4. Delete a course\n" +

            "5. Back to Home";


    String TRAINEE_OPTIONS = "Choose one of the following options (Type a number from 1 to 7):\n" +

            "1. Register\n" +

            "2. Login\n" +

            "3. Show all courses\n" +

            "4. Show enrolled courses\n" +

            "5. Enrolled in a course\n" +

            "6. Unenrolled from a course\n" +

            "7. Back to Home";
}
